import React, { Component } from 'react';
import './App.css';
import Search from './Search.js'
import {Media} from 'reactstrap';
import axios from 'axios';
class App extends Component {
  constructor(props) {
    super(props);
    this.state={
      cityid: '44418',
      location: 'Ciudad',
      weather: 'Estado del Clima',
      temp: 'Tempertura Minima',
      url_img: 'https://www.metaweather.com/static/img/weather/png/64/h.png',
    };
    this.onSubmitCity=this.onSubmitCity.bind(this);
  }
  onSubmitCity =(city) =>{
    this.componentDidMount(city);
  };
  componentDidMount(city) {
    console.log('buscando');
    console.log(`${city}`);
    axios.get(`https://www.metaweather.com/api/location/search/?query=${city}`)
      .then(res => {
        const info = res.data;
        this.setState({ cityid: info.woeid, city: info.tittle });
      }).catch(() => console.log('Error no se encontro la ciudad'))
    if(this.state.cityid){
      axios.get(`https://www.metaweather.com/api/location/${this.state.cityid}`)
      .then(res => {
        const info = res.data;
        this.setState({url_img: `https://www.metaweather.com/static/img/weather/png/64/${info.consolidated_weather[0].weather_state_abbr}.png`,location: info.title ,weather: info.consolidated_weather[0].weather_state_name, temp: info.consolidated_weather[0].the_temp });
      }).catch(() => console.log('Error no se puede cargar el clima'));
    }
  }
  


  render() {
    return (
      <div className='container'>
        <div className='row '>
          <div className='col-xs-6 col-sm-2'></div>
          <div className='col-xs-6 col-sm-8 ' align="center">
            <Media object middle src={this.state.url_img}  alt="Clima" />
           
            <h1 className='text-center'>{this.state.location}</h1>
            <h2 className='text-center'>{this.state.weather}</h2>
            <h2 className='text-center'>{this.state.temp} °C</h2>
            <Search onSubmitCity={this.onSubmitCity}/>
          </div>
          <div className='col-xs-6 col-sm-2'></div>
        </div>
       

      </div>
    );
  }
}

export default App;
