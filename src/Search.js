import { Button, Form, FormGroup, Input} from 'reactstrap';
import React from 'react';

const Search = ({onSubmitCity}) => {
    const handleSubmit= (e) =>{
      e.preventDefault();
      const city=e.target.city.value;
      //console.log(city);
      if (onSubmitCity && city) {
        onSubmitCity(city);
      }
      e.target.city.value= null;
    };

    

    return(
      
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Input type="search"  name="city" id="exampleSearch" placeholder="search placeholder" />
        </FormGroup>
      <Button  type="submit" >Search</Button>
    </Form>
    );
};

export default Search;